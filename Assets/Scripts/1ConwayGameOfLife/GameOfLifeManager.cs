﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameOfLifeManager : MonoBehaviour
{
    private const int MIN_GRID_SIZE = 3;
    private const int MIN_CAM_SIZE = 2;

    [SerializeField] private Camera _mainCamera = null;
    [SerializeField] private CellManager _cellManager = null;
    [Header("Generate board")]
    [SerializeField] private Button _generateBoardBtn = null;
    [SerializeField] private TMP_InputField _cellSizeXText = null;
    [SerializeField] private TMP_InputField _cellSizeYText = null;
    [Header("Clear board")]
    [SerializeField] private Button _clearBoardBtn = null;
    [Header("Simulate board")]
    [SerializeField] private Button _simulateBoardBtn = null;

    public bool Interactable = true;

    private void Awake()
    {
        _generateBoardBtn.onClick.AddListener(OnGenerateButtonClicked);
        _clearBoardBtn.onClick.AddListener(OnClearButtonClicked);
        _simulateBoardBtn.onClick.AddListener(OnSimulateButtonClicked);
    }

    private void OnDestroy()
    {
        _generateBoardBtn.onClick.RemoveListener(OnGenerateButtonClicked);
        _clearBoardBtn.onClick.RemoveListener(OnClearButtonClicked);
        _simulateBoardBtn.onClick.RemoveListener(OnSimulateButtonClicked);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && Interactable)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                Cell cell = hit.transform.GetComponent<Cell>();
                cell.ChangeAliveStatus(!cell.IsAlive);
            }
        }
    }

    private void OnGenerateButtonClicked()
    {
        int x = int.Parse(_cellSizeXText.text);
        int y = int.Parse(_cellSizeYText.text);

        if(x < MIN_GRID_SIZE || y < MIN_GRID_SIZE)
        {
            return;
        }

        int combinedSize = MIN_CAM_SIZE - MIN_GRID_SIZE;
        _cellManager.GenerateCells(x, y);
        _mainCamera.orthographicSize = x > y ? combinedSize + x : combinedSize + y;
    }

    private void OnClearButtonClicked()
    {
        _cellManager.ClearBoard();
    }

    private void OnSimulateButtonClicked()
    {
        _cellManager.SimulateBoard();
    }
}
