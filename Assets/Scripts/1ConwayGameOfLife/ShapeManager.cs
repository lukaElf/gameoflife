﻿using Array2DEditor;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GameOfLifeManager))]
public class ShapeManager : MonoBehaviour
{
    enum Shape
    {
        StillLife,
        Oscillator,
        Spaceship
    }

    [Header("Predefined shapes")]
    [SerializeField] private PremadeShape _stillLifeShape = null;
    [SerializeField] private PremadeShape _oscillatorShape = null;
    [SerializeField] private PremadeShape _spaceshipShape = null;
    [Header("Toggles")]
    [SerializeField] private Toggle _stillLifeToggle = null;
    [SerializeField] private Toggle _oscillatorToggle = null;
    [SerializeField] private Toggle _spaceshipToggle = null;
    [Header("Cell Manager")]
    [SerializeField] private CellManager _cellManager = null;

    private GameOfLifeManager _golManager = null;

    private bool _draggingEnabled;
    private Shape _currentShape;
    private bool _draggingShape;
    private Cell _lastHighlightedCell;
    private Cell[,] _currentGrid;
    private int _currentX = 0;
    private int _currentY = 0;

    private void Awake()
    {
        _stillLifeToggle.onValueChanged.AddListener(OnStillLifeToggled);
        _oscillatorToggle.onValueChanged.AddListener(OnOscillatorToggled);
        _spaceshipToggle.onValueChanged.AddListener(OnSpaceshipToggled);
        _golManager = GetComponent<GameOfLifeManager>();
    }

    private void OnDestroy()
    {
        _stillLifeToggle.onValueChanged.RemoveListener(OnStillLifeToggled);
        _oscillatorToggle.onValueChanged.RemoveListener(OnOscillatorToggled);
        _spaceshipToggle.onValueChanged.RemoveListener(OnSpaceshipToggled);
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0) && _draggingShape)
        {
            ResetToggles();
            ResetAllCells();
            _lastHighlightedCell = null;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                ApplyShape(_currentShape);
            }
            _golManager.Interactable = true;
        }

        if (Input.GetMouseButtonDown(1) && _draggingShape)
        {
            ResetToggles();
            ResetAllCells();
            _lastHighlightedCell = null;
            _golManager.Interactable = true;
        }

        if(_draggingShape)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                Cell cell = hit.transform.GetComponent<Cell>();

                if (_lastHighlightedCell != cell)
                {
                    _lastHighlightedCell = cell;
                    ResetAllCells();

                    for (int i = 0; i < _currentGrid.GetLength(0); i++)
                    {
                        for (int j = 0; j < _currentGrid.GetLength(1); j++)
                        {
                            if (cell == _currentGrid[i, j])
                            {
                                _currentX = i;
                                _currentY = j;
                                HighlightShape(_currentShape);
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                _lastHighlightedCell = null;
                ResetAllCells();
            }
        }

        if(_draggingEnabled)
        {
            _draggingEnabled = false;
            _draggingShape = true;
        }
    }

    private void ResetToggles()
    {
        _draggingShape = false;
        _stillLifeToggle.SetIsOnWithoutNotify(false);
        _oscillatorToggle.SetIsOnWithoutNotify(false);
        _spaceshipToggle.SetIsOnWithoutNotify(false);
    }

    private void ResetAllCells()
    {
        for (int i = 0; i < _currentGrid.GetLength(0); i++)
        {
            for (int j = 0; j < _currentGrid.GetLength(1); j++)
            {
                Cell cell = _currentGrid[i, j];
                cell.ResetHighlight();
            }
        }
    }

    private void OnStillLifeToggled(bool isOn)
    {
        if(isOn)
        {
            _currentShape = Shape.StillLife;
            Toggled();
        }
    }

    private void OnOscillatorToggled(bool isOn)
    {
        if (isOn)
        {
            _currentShape = Shape.Oscillator;
            Toggled();
        }
    }

    private void OnSpaceshipToggled(bool isOn)
    {
        if (isOn)
        {
            _currentShape = Shape.Spaceship;
            Toggled();
        }
    }

    private void Toggled()
    {
        _draggingEnabled = true;
        _currentGrid = _cellManager.GetCurrentGrid();
        _golManager.Interactable = false;
    }

    private void HighlightShape(Shape shape)
    {
        switch (shape)
        {
            case Shape.StillLife:
                Highlight(_stillLifeShape.Shape);
                break;
            case Shape.Oscillator:
                Highlight(_oscillatorShape.Shape);
                break;
            case Shape.Spaceship:
                Highlight(_spaceshipShape.Shape);
                break;
        }
    }

    private void Highlight(Array2DBool shape)
    {
        int sizeX = _currentGrid.GetLength(0);
        int sizeY = _currentGrid.GetLength(1);

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                bool shown = shape.GetCell(x + 1, y + 1);
                Cell cell = _currentGrid[GetCurrentCellIndex(sizeX, _currentX, x), GetCurrentCellIndex(sizeY, _currentY, y)];
                if (shown)
                {
                    cell.SetHighlighted();
                }
                else
                {
                    cell.SetDeadHighlighted();
                }
            }
        }
    }

    private void ApplyShape(Shape shape)
    {
        switch (shape)
        {
            case Shape.StillLife:
                ApplyShape(_stillLifeShape.Shape);
                break;
            case Shape.Oscillator:
                ApplyShape(_oscillatorShape.Shape);
                break;
            case Shape.Spaceship:
                ApplyShape(_spaceshipShape.Shape);
                break;
        }
    }

    private void ApplyShape(Array2DBool shape)
    {
        int sizeX = _currentGrid.GetLength(0);
        int sizeY = _currentGrid.GetLength(1);

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                bool shown = shape.GetCell(x + 1, y + 1);
                Cell cell = _currentGrid[GetCurrentCellIndex(sizeX, _currentX, x), GetCurrentCellIndex(sizeY, _currentY, y)];
                cell.ChangeAliveStatus(shown);
            }
        }
    }

    private int GetCurrentCellIndex(int size, int i, int x)
    {
        if (i % size == 0 && x == -1)
        {
            return size - 1;
        }
        else if (i % size == size - 1 && x == 1)
        {
            return 0;
        }

        return i + x;
    }
}
