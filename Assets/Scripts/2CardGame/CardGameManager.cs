using DG.Tweening;
using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardGameManager : MonoBehaviour
{
    public static Action StartingNewGame;
    public static Action<int> NextPlayerTurn;
    public static Action<int, int> UpdateScore;

    private const int MIN_GRID_SIZE = 3;
    private const int MIN_CAM_SIZE = 2;
    private const int GAME_TURNS = 6;

    [Header("Buttons")]
    [SerializeField] private Button _startGameButton = null;
    [SerializeField] private Button _nextTurnButton = null;
    [Header("Board size")]
    [SerializeField] private Camera _mainCamera = null;
    [SerializeField] private CellManager _cellManager = null;
    [SerializeField] private int _boardSizeX = 50;
    [SerializeField] private int _boardSizeY = 50;
    [Header("Simulate")]
    [SerializeField] private int _simulateTurns = 50;
    [SerializeField] private float _simulateDelay = 0.01f;
    [Header("Player")]
    [SerializeField] private int _numberOfPlayers = 1;
    [SerializeField] private bool _isSingleplayer = true;
    [SerializeField] private BoardInteractionManager _boardInteractionManager = null;
    [SerializeField] private Player[] _players = null;
    [Header("Turns")]
    [SerializeField] private CanvasGroup _turnCg = null;
    [SerializeField] private TextMeshProUGUI _turnOrderLabel = null;
    [Header("End score")]
    [SerializeField] private CanvasGroup _scoreCg = null;
    [SerializeField] private TextMeshProUGUI _scoreLabel = null;

    private bool _isGameInProgress = false;
    private bool _simulatingBoard = false;

    private WaitForSeconds _waitForSeconds;
    private Coroutine _coroutine;

    public static int CurrentPlayer { get; private set; } = 1;
    private int _currentTurn = 0;
    private int _finalScore = 0;

    private void Awake()
    {
        _waitForSeconds = new WaitForSeconds(_simulateDelay);

        _startGameButton.onClick.AddListener(StartNewGame);
        _nextTurnButton.onClick.AddListener(NewTurn);

        _boardInteractionManager.SetMode(_isSingleplayer);
        _cellManager.SetMode(_isSingleplayer);
        if(_isSingleplayer)
        {
            foreach(Player player in _players)
            {
                player.HideScore();
            }
        }
    }

    private void OnDestroy()
    {
        _startGameButton.onClick.RemoveListener(StartNewGame);
        _nextTurnButton.onClick.RemoveListener(NewTurn);
    }

    private void StartNewGame()
    {
        if(_coroutine != null)
        {
            StopCoroutine(_coroutine);
            _coroutine = null;
        }

        ResetGame();
        StartingNewGame?.Invoke();
        CreateNewBoard();
        ShowNextTurn();
        _isGameInProgress = true;
    }

    private void ResetGame()
    {
        _simulatingBoard = false;
        _scoreCg.alpha = 0f;
        CurrentPlayer = 1;
        _currentTurn = 1;
    }

    private void CreateNewBoard()
    {
        int combinedSize = MIN_CAM_SIZE - MIN_GRID_SIZE;
        _cellManager.GenerateCells(_boardSizeX, _boardSizeY);
        _mainCamera.orthographicSize = _boardSizeX > _boardSizeY ? combinedSize + _boardSizeX : combinedSize + _boardSizeY;
    }

    private void NewTurn()
    {
        if(!_isGameInProgress || _simulatingBoard)
        {
            return;
        }

        _simulatingBoard = true;
        _coroutine = StartCoroutine(SimulateBoard_Coroutine());
    }

    private IEnumerator SimulateBoard_Coroutine()
    {
        bool isGameFinished = false;
        bool canSimulateBoard = false;
        CurrentPlayer++;
        if (CurrentPlayer > _numberOfPlayers)
        {
            CurrentPlayer = 1;
            canSimulateBoard = true;
        }

        if(canSimulateBoard)
        {
            for (int i = 0; i < _simulateTurns; i++)
            {
                _cellManager.SimulateBoard();

                yield return _waitForSeconds;
            }

            _currentTurn++;
            if(_currentTurn > GAME_TURNS)
            {
                isGameFinished = true;
            }

            if (!_isSingleplayer)
            {
                _players[0].UpdateScore(_cellManager.GetScore(1));
                _players[1].UpdateScore(_cellManager.GetScore(2));
            }
        }


        if (!isGameFinished)
        {
            DrawNewCards();
            ShowNextTurn();
        }
        else
        {
            ShowFinalScore();
            _isGameInProgress = false;
        }

        _simulatingBoard = false;
        _coroutine = null;
    }

    private void ShowNextTurn()
    {
        _turnOrderLabel.text = "Turn " + _currentTurn + "\n" + "Player " + CurrentPlayer;
        _turnCg.DOFade(1, 0.5f).OnComplete(() =>
        {
            _turnCg.DOFade(0, 0.5f).SetDelay(2f);
        });
    }

    public void DrawNewCards()
    {
        NextPlayerTurn?.Invoke(CurrentPlayer);
    }

    private void ShowFinalScore()
    {
        if(_isSingleplayer)
        {
            _finalScore = 0;
            Cell[,] grid = _cellManager.GetCurrentGrid();

            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int j = 0; j < grid.GetLength(1); j++)
                {
                    if (grid[i, j].IsAlive)
                    {
                        _finalScore++;
                    }
                }
            }
            _scoreLabel.text = "Score: " + _finalScore;
        }
        else
        {
            int player1Score = _players[0].GetScore();
            int player2Score = _players[1].GetScore();

            if(player1Score > player2Score)
            {
                _scoreLabel.text = "Player 1 wins!\n" + "Score: " + player1Score;
            }
            else if(player1Score < player2Score)
            {
                _scoreLabel.text = "Player 2 wins!\n" + "Score: " + player2Score;
            }
            else
            {
                _scoreLabel.text = "Tie!";
            }
        }

        _scoreCg.DOFade(1, 0.5f);
    }
}
