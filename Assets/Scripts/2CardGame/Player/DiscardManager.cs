using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DiscardManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _discardText = null;

    private int _currentCardsInDiscard = 0;

    private List<PremadeShape> _discardList;

    private void Awake()
    {
        _discardList = new List<PremadeShape>();
    }

    public void ResetDiscard()
    {
        _discardList.Clear();
        _currentCardsInDiscard = 0;
        _discardText.text = _currentCardsInDiscard.ToString();
    }

    public void UpdateDiscard(Card card)
    {
        _currentCardsInDiscard++;
        _discardText.text = _currentCardsInDiscard.ToString();
        _discardList.Add(card.Shape);
    }

    public List<PremadeShape> GetDiscard()
    {
        return _discardList;
    }
}
