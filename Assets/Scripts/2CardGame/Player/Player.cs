using TMPro;
using UnityEngine;

public class Player : MonoBehaviour
{
    private const int START_GAME_DRAW = 3;
    private const int TURN_DRAW = 1;
    private const int ACTIONS_PER_TURN = 4;

    [SerializeField] private DeckManager _deckManager = null;
    [SerializeField] private PlayerHand _playerHand = null;
    [SerializeField] private DiscardManager _discard = null;
    [SerializeField] private int _playerIndex = 1;
    [SerializeField] private TextMeshProUGUI _scoreText = null;

    private int _currentScore = 0;

    private void Awake()
    {
        CardGameManager.StartingNewGame += OnNewGameStarted;
        CardGameManager.NextPlayerTurn += OnNewTurnStarted;
    }

    private void OnDestroy()
    {
        CardGameManager.StartingNewGame -= OnNewGameStarted;
        CardGameManager.NextPlayerTurn -= OnNewTurnStarted;
    }

    private void OnNewGameStarted()
    {
        UpdateScore(0);
        _playerHand.ClearHand();
        _discard.ResetDiscard();
        _deckManager.CreateDeck();
        _deckManager.DrawCards(START_GAME_DRAW);
        _playerHand.UpdateNumberOfActions(ACTIONS_PER_TURN);
    }

    private void OnNewTurnStarted(int playerIndex)
    {
        if(playerIndex != _playerIndex)
        {
            return;
        }

        _deckManager.DrawCards(TURN_DRAW);
        _playerHand.UpdateNumberOfActions(ACTIONS_PER_TURN);
    }

    public int GetPlayerIndex()
    {
        return _playerIndex;
    }

    public void UpdateScore(int score)
    {
        _currentScore = score;
        _scoreText.text = "Score: " + _currentScore.ToString();
    }

    public void HideScore()
    {
        _scoreText.enabled = false;
    }

    public int GetScore()
    {
        return _currentScore;
    }
}
