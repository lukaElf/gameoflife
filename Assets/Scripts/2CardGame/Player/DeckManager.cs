using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DeckManager : MonoBehaviour
{
    private const int StillLifeNumberOfCards = 5;
    private const int OscillatorNumberOfCards = 3;
    private const int SpaceshipNumberOfCards = 2;

    private const int DrawCardsNum = 3;

    public Action<Card> CardDrawn;

    [Header("Shapes")]
    [SerializeField] private PremadeShape[] _stillLifeShapes = null;
    [SerializeField] private PremadeShape[] _oscillatorShapes = null;
    [SerializeField] private PremadeShape[] _spaceshipShapes = null;
    [Header("Player")]
    [SerializeField] private Player _player = null;
    [Header("Card")]
    [SerializeField] private Card _cardPrefab = null;
    [SerializeField] private Transform _playerHand = null;
    [Header("Label")]
    [SerializeField] private TextMeshProUGUI _deckText = null;
    [Header("Discard")]
    [SerializeField] private DiscardManager _discard = null;

    private List<PremadeShape> _cardList;

    private void Awake()
    {
        _cardList = new List<PremadeShape>();
    }

    public void CreateDeck()
    {
        _cardList.Clear();

        for (int i = 0; i < StillLifeNumberOfCards; i++)
        {
            _cardList.Add(_stillLifeShapes[UnityEngine.Random.Range(0, _stillLifeShapes.Length)]);
        }

        for (int i = 0; i < OscillatorNumberOfCards; i++)
        {
            _cardList.Add(_oscillatorShapes[UnityEngine.Random.Range(0, _oscillatorShapes.Length)]);
        }

        for (int i = 0; i < SpaceshipNumberOfCards; i++)
        {
            _cardList.Add(_spaceshipShapes[UnityEngine.Random.Range(0, _spaceshipShapes.Length)]);
        }

        _cardList.Shuffle();

        UpdateDeckText(_cardList.Count);

        _discard.ResetDiscard();
    }

    private void UpdateDeckText(int currentDeckSize)
    {
        _deckText.text = currentDeckSize.ToString();
    }

    public void DrawCards(int numberOfCardsDrawn)
    {
        for(int i = 0; i < numberOfCardsDrawn; i++)
        {
            if(_cardList.Count == 0)
            {
                _cardList = new List<PremadeShape>(_discard.GetDiscard());
                _cardList.Shuffle();
                _discard.ResetDiscard();
            }

            Card card = Instantiate(_cardPrefab);
            card.SetupCard(_cardList[0], _player.GetPlayerIndex());
            _cardList.RemoveAt(0);
            CardDrawn?.Invoke(card);
        }

        UpdateDeckText(_cardList.Count);
    }
}
