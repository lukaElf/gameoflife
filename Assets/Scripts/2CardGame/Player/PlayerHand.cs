using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerHand : MonoBehaviour
{
    public static Action<Card> CardSelected;

    [SerializeField] private Player _player = null;
    [SerializeField] private DeckManager _deckManager = null;
    [SerializeField] private DiscardManager _discardManager = null;
    [SerializeField] private Transform _playerHand = null;
    [SerializeField] private TextMeshProUGUI _actionLabel = null;

    private List<Card> _cards;
    private int _currentActions = 4;

    private void Awake()
    {
        _cards = new List<Card>();
        _deckManager = GetComponent<DeckManager>();

        _deckManager.CardDrawn += OnCardDrawn;
        BoardInteractionManager.CardUsed += OnCardUsed;
    }

    private void OnDestroy()
    {
        _deckManager.CardDrawn -= OnCardDrawn;
        BoardInteractionManager.CardUsed -= OnCardUsed;
    }

    public void ClearHand()
    {
        for(int i =0; i < _cards.Count; i++)
        {
            _cards[i].CardSelected -= OnCardSelected;
            Destroy(_cards[i].gameObject);
        }

        _cards.Clear();
    }

    private void OnCardDrawn(Card card)
    {
        card.transform.SetParent(_playerHand);
        card.CardSelected += OnCardSelected;
        _cards.Add(card);
    }

    private void OnCardSelected(Card card)
    {
        if (CardGameManager.CurrentPlayer != _player.GetPlayerIndex() || !IsPlayerCard(card))
        {
            return;
        }

        if (card.GetActionCost() > _currentActions)
        {
            card.SetInvalid();
            return;
        }

        card.SetHighlighted();
        CardSelected?.Invoke(card);
    }

    public void UpdateNumberOfActions(int actions)
    {
        _currentActions = actions;
        _actionLabel.text = "Actions: " + _currentActions;
    }

    private bool IsPlayerCard(Card card)
    {
        return _cards.Contains(card);
    }

    private void OnCardUsed(Card card)
    {
        if(CardGameManager.CurrentPlayer == _player.GetPlayerIndex() && _cards.Contains(card))
        {
            _currentActions -= card.GetActionCost();
            _actionLabel.text = "Actions: " + _currentActions;
            _discardManager.UpdateDiscard(card);
            _cards.Remove(card);
            Destroy(card.gameObject);
        }
    }
}
