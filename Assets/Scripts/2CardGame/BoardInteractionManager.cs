using Array2DEditor;
using System;
using UnityEngine;

public class BoardInteractionManager : MonoBehaviour
{
    public static Action<Card> CardUsed;

    [SerializeField] private CellManager _cellManager = null;
    private bool _isSingleplayer;

    private Card _selectedCard;
    private Cell _lastHighlightedCell;
    private Cell[,] _currentGrid;
    private int _currentX = 0;
    private int _currentY = 0;

    private void Awake()
    {
        CellManager.BoardGenerated += OnBoardGenerated;
        PlayerHand.CardSelected += OnCardSelected;
    }

    private void OnDestroy()
    {
        CellManager.BoardGenerated -= OnBoardGenerated;
        PlayerHand.CardSelected -= OnCardSelected;
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0) && _selectedCard != null)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                ResetAllCells();
                _lastHighlightedCell = null;
                ApplyShape(_selectedCard.GetShape(), _selectedCard.GetOwner());
                CardUsed?.Invoke(_selectedCard);
                _selectedCard = null;
            }
        }

        if (Input.GetMouseButtonDown(1) && _selectedCard != null)
        {
            ResetAllCells();
            _selectedCard.SetNormal();
            _lastHighlightedCell = null;
            _selectedCard = null;
        }

        if (_selectedCard != null)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                Cell cell = hit.transform.GetComponent<Cell>();

                if (_lastHighlightedCell != cell)
                {
                    _lastHighlightedCell = cell;
                    ResetAllCells();

                    for (int i = 0; i < _currentGrid.GetLength(0); i++)
                    {
                        for (int j = 0; j < _currentGrid.GetLength(1); j++)
                        {
                            if (cell == _currentGrid[i, j])
                            {
                                _currentX = i;
                                _currentY = j;
                                Highlight(_selectedCard.GetShape());
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                _lastHighlightedCell = null;
                ResetAllCells();
            }
        }
    }

    private void OnBoardGenerated()
    {
        _currentGrid = _cellManager.GetCurrentGrid();
    }

    private void OnCardSelected(Card card)
    {
        if(_selectedCard != null)
        {
            _selectedCard.SetNormal();
        }

        _selectedCard = card;
    }

    private void ResetAllCells()
    {
        for (int i = 0; i < _currentGrid.GetLength(0); i++)
        {
            for (int j = 0; j < _currentGrid.GetLength(1); j++)
            {
                Cell cell = _currentGrid[i, j];
                cell.ResetHighlight();
            }
        }
    }

    private void Highlight(Array2DBool shape)
    {
        int sizeX = _currentGrid.GetLength(0);
        int sizeY = _currentGrid.GetLength(1);

        float offsetX = shape.GridSize.x % 2 == 0 ? -1 : 0;
        float offsetY = shape.GridSize.y % 2 == 0 ? -1 : 0;

        for (int x = -shape.GridSize.x / 2; x <= (shape.GridSize.x / 2) + offsetX; x++)
        {
            for (int y = -shape.GridSize.y / 2; y <= (shape.GridSize.y / 2) + offsetY; y++)
            {
                bool shown = shape.GetCell(x + shape.GridSize.x / 2, y + shape.GridSize.y / 2);
                Cell cell = _currentGrid[GetCurrentCellIndex(sizeX, _currentX, x), GetCurrentCellIndex(sizeY, _currentY, y)];
                if (shown)
                {
                    cell.SetHighlighted();
                }
                else
                {
                    cell.SetDeadHighlighted();
                }
            }
        }
    }

    private void ApplyShape(Array2DBool shape, int playerIndex)
    {
        int sizeX = _currentGrid.GetLength(0);
        int sizeY = _currentGrid.GetLength(1);

        float offsetX = shape.GridSize.x % 2 == 0 ? -1 : 0;
        float offsetY = shape.GridSize.y % 2 == 0 ? -1 : 0;

        for (int x = -shape.GridSize.x / 2; x <= (shape.GridSize.x / 2) + offsetX; x++)
        {
            for (int y = -shape.GridSize.y / 2; y <= (shape.GridSize.y / 2) + offsetY; y++)
            {
                bool shown = shape.GetCell(x + shape.GridSize.x / 2, y + shape.GridSize.y / 2);
                Cell cell = _currentGrid[GetCurrentCellIndex(sizeX, _currentX, x), GetCurrentCellIndex(sizeY, _currentY, y)];
                cell.ChangeAliveStatus(shown);
                if(!_isSingleplayer)
                {
                    cell.SetOwner(playerIndex);
                }
            }
        }
    }

    private int GetCurrentCellIndex(int size, int i, int x)
    {
        if(i + x < 0)
        {
            return size + x;
        }

        if(i + x >= size)
        {
            return -1 + x;
        }

        return i + x;
    }

    public void SetMode(bool isSingleplayer)
    {
        _isSingleplayer = isSingleplayer;
    }
}
