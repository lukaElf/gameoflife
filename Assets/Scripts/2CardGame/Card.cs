using Array2DEditor;
using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Card : MonoBehaviour, IPointerClickHandler
{
    public Action<Card> CardSelected;

    [SerializeField] private GameObject _tilePrefab = null;
    [SerializeField] private GridLayoutGroup _gridLayout = null;
    [SerializeField] private RectTransform _rectTransform = null;
    [SerializeField] private Image _bg = null;
    [SerializeField] private int _owner = 0;

    public PremadeShape Shape;

    private Tween _tween;

    public void OnPointerClick(PointerEventData eventData)
    {
        CardSelected?.Invoke(this);
    }

    public void SetupCard(PremadeShape shape, int owner)
    {
        _owner = owner;
        Shape = shape;

        float gridSizeX = (_rectTransform.sizeDelta.x - _gridLayout.padding.left - _gridLayout.padding.right - ((Shape.Shape.GridSize.x - 1) * _gridLayout.spacing.x)) / Shape.Shape.GridSize.x;
        float gridSizeY = (_rectTransform.sizeDelta.y - _gridLayout.padding.bottom - _gridLayout.padding.top - ((Shape.Shape.GridSize.y - 1) * _gridLayout.spacing.y)) / Shape.Shape.GridSize.y;
        float universalSize = gridSizeX <= gridSizeY ? gridSizeX : gridSizeY;
        _gridLayout.cellSize = new Vector2(universalSize, universalSize);

        for (int j = 0; j < Shape.Shape.GridSize.y; j++)
        {
            for (int i = 0; i < Shape.Shape.GridSize.x; i++)
            {
                GameObject tile = Instantiate(_tilePrefab, transform);
                tile.GetComponent<Image>().enabled = Shape.Shape.GetCell(i, j);
            }
        }

        name = "Card " + Shape.name;
    }

    public int GetActionCost()
    {
        return Shape.ActionCost;
    }

    public Array2DBool GetShape()
    {
        return Shape.Shape;
    }

    public void SetHighlighted()
    {
        _bg.color = Color.green;
    }

    public void SetNormal()
    {
        _bg.color = Color.white;
    }

    public void SetInvalid()
    {
        _tween?.Kill();
        _bg.color = Color.red;
        _tween = _bg.DOColor(Color.white, 0.5f).SetDelay(1f);
    }

    public int GetOwner()
    {
        return _owner;
    }
}
