using Array2DEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "Shape", menuName = "ScriptableObjects/GameOfLifeShape", order = 0)]
public class PremadeShape : ScriptableObject
{
    [SerializeField] public Array2DBool Shape = null;
    [SerializeField] public int ActionCost = 0;
}
