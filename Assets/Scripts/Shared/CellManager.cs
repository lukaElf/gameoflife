﻿using System;
using UnityEngine;

public class CellManager : MonoBehaviour
{
    public static Action BoardGenerated; 

    [SerializeField] private Cell _cellPrefab = null;
    [SerializeField] private Transform _board = null;
    [SerializeField] private float _xOffset = 0.5f;
    [SerializeField] private float _yOffset = 0.5f;
    [SerializeField] private int _deadCellPercentage = 25;
    private bool _isSingleplayer;

    private Cell[,] _grid;
    private int _currentXSize;
    private int _currentYSize;

    public void GenerateCells(int sizeX, int sizeY)
    {
        ClearBoard();
        _currentXSize = sizeX;
        _currentYSize = sizeY;

        _grid = new Cell[_currentXSize, _currentYSize];
        for (int i = 0; i < _currentXSize; i++)
        {
            for (int j = 0; j < _currentYSize; j++)
            {
                _grid[i, j] = Instantiate(_cellPrefab, _board);
                _grid[i, j].transform.localPosition = new Vector3(i + (-_currentXSize / 2) + CalculateXOffset(), _currentYSize / 2 + CalculateYOffset() - j);
                _grid[i, j].Setup(_deadCellPercentage);
            }
        }

        BoardGenerated?.Invoke();
    }

    private float CalculateXOffset()
    {
        if (_currentXSize % 2 == 0)
        {
            return _xOffset;
        }
        else
        {
            return 0.0f;
        }
    }

    private float CalculateYOffset()
    {
        if (_currentYSize % 2 == 0)
        {
            return -_yOffset;
        }
        else
        {
            return 0.0f;
        }
    }

    public void ClearBoard()
    {
        if (_grid != null)
        {
            for (int i = 0; i < _currentXSize; i++)
            {
                for (int y = 0; y < _currentYSize; y++)
                {
                    Destroy(_grid[i, y].gameObject);
                }
            }
        }
    }

    public void SimulateBoard()
    {
        for (int i = 0; i < _currentXSize; i++)
        {
            for (int j = 0; j < _currentYSize; j++)
            {
                Cell cell = _grid[i, j];

                int aliveCells = 0;
                int player1AliveCells = 0;
                int player2AliveCells = 0;

                player1AliveCells += cell.IsAlive && cell.GetOwner() == 1 ? 1 : 0;
                player2AliveCells += cell.IsAlive && cell.GetOwner() == 2 ? 1 : 0;

                for (int x = -1; x <= 1; x++)
                {
                    for (int y = -1; y <= 1; y++)
                    {
                        if (x == 0 && y == 0)
                        {
                            continue;
                        }

                        Cell neighbourCell = _grid[GetCurrentCellIndex(_currentXSize, i, x), GetCurrentCellIndex(_currentYSize, j, y)];

                        player1AliveCells += neighbourCell.IsAlive && neighbourCell.GetOwner() == 1 ? 1 : 0;
                        player2AliveCells += neighbourCell.IsAlive && neighbourCell.GetOwner() == 2 ? 1 : 0;
                        aliveCells += neighbourCell.IsAlive ? 1 : 0;
                    }
                }

                bool isAliveInNextGeneration = (cell.IsAlive && aliveCells >= 2 && aliveCells <= 3) || (!cell.IsAlive && aliveCells == 3);
                if(!_isSingleplayer)
                {
                    if (player1AliveCells > player2AliveCells && isAliveInNextGeneration)
                    {
                        cell.SetNextStatus(isAliveInNextGeneration, 1);
                    }
                    else if(player2AliveCells > player1AliveCells && isAliveInNextGeneration)
                    {
                        cell.SetNextStatus(isAliveInNextGeneration, 2);
                    }
                    else
                    {
                        cell.SetNextStatus(isAliveInNextGeneration, cell.GetOwner());
                    }
                }
                else
                {
                    cell.SetNextStatus(isAliveInNextGeneration, 0);
                }

            }
        }

        ApplyNextStatusForAllCells();
    }

    private int GetCurrentCellIndex(int size, int i, int x)
    {
        if (i % size == 0 && x == -1)
        {
            return size - 1;
        }
        else if(i % size == size - 1 && x == 1)
        {
            return 0;
        }

        return i + x;
    }

    private void ApplyNextStatusForAllCells()
    {
        for (int i = 0; i < _currentXSize; i++)
        {
            for (int j = 0; j < _currentYSize; j++)
            {
                Cell cell = _grid[i, j];
                cell.ApplyNextStatus();
            }
        }
    }

    public Cell[,] GetCurrentGrid()
    {
        return _grid;
    }

    public int GetScore(int player)
    {
        int score = 0;
        for (int i = 0; i < _currentXSize; i++)
        {
            for (int j = 0; j < _currentYSize; j++)
            {
                score += _grid[i, j].IsAlive && _grid[i, j].GetOwner() == player ? 1 : 0;
            }
        }

        return score;
    }

    public void SetMode(bool isSingleplayer)
    {
        _isSingleplayer = isSingleplayer;
    }
}
