﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Cell : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _renderer = null;
    [SerializeField] private bool _isAlive = false;
    [SerializeField] private Color[] _aliveColors = null;
    [SerializeField] private Color _deadColor = Color.black;
    [SerializeField] private Color _highlightedColor = Color.black;

    private bool _nextAliveStatus;
    private int _nextOwner;
    [SerializeField] private int _owner = 0;
    private Color _currentNormalColor = Color.white;

    public bool IsAlive
    {
        get => _isAlive;
        set => _isAlive = value;
    }

    public void Setup(int percentage)
    {
        IsAlive = Random.Range(0, 100) <= percentage;
        ChangeAliveStatus(IsAlive);
    }

    public void ChangeAliveStatus(bool isAlive)
    {
        IsAlive = isAlive;

        _renderer.color = IsAlive ? _aliveColors[_owner] : _deadColor;
    }

    public void SetHighlighted()
    {
        _renderer.color = _highlightedColor;
    }

    public void SetDeadHighlighted()
    {
        _renderer.color = _deadColor;
    }

    public void ResetHighlight()
    {
        _renderer.color = IsAlive ? _aliveColors[_owner] : _deadColor;
    }

    public void SetNextStatus(bool isAlive, int newOwner)
    {
        _nextAliveStatus = isAlive;
        _nextOwner = newOwner;
    }

    public void ApplyNextStatus()
    {
        ChangeAliveStatus(_nextAliveStatus);
        SetOwner(_nextOwner);
    }

    public void SetOwner(int newOwner)
    {
        if (IsAlive && _owner != newOwner)
        {
            _owner = newOwner;
            _renderer.color = _aliveColors[_owner];
        }
    }

    public int GetOwner()
    {
        return _owner;
    }
}
